/*
 * math.h
 *
 *  Created on: April 27, 2017
 *      Author: MOpenlander
 */

#ifndef MATH_H_
#define MATH_H_

#include <vector>
#include <string>


namespace Math {
    
    /******************************** Math ********************************/
    extern double sigMoid(double v);
    extern int factorial(int n);
    extern int combination(int n, int r);
    extern void twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A);
    extern double unitStep(double X);
    extern std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B);
    //extern double expm (double p, double ak);
    //extern double series(int m, int id);
    //extern double piNumber(int sampleNumber);
    extern std::vector<double> decToBin(double n, int numDigits);

    /******************************** End Math ********************************/

};

#endif /* MATH_H_ */