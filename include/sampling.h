/*
 * sampling.h
 *
 *  Created on: April 27, 2017
 *      Author: MOpenlander
 */

#ifndef SAMPLING_H_
#define SAMPLING_H_

#include <vector>
#include <string>
#include <fstream>

#include "MTRand.h"


namespace Sampling {
    
    /******************************** Sampling ********************************/
     extern std::ifstream piFin;
    extern std::vector < std::vector < double > > sobol_points(unsigned N, unsigned D);
    extern double *single_sobol_points(unsigned N, unsigned D);

    extern std::vector < std::vector < double > > faureSampling(int Nd, int Ns);
    extern std::vector<int> changeBase(double num, double base, int numDigits);
    extern double corputBase(double base, double number); //TODO Start Here

    extern std::vector < std::vector < double > > latinHyperCube_Random(int numVars, int numSamples, MTRand& mt);
    extern std::vector < std::vector < double > > descriptiveSampling_Random(int numVars, int numSamples, MTRand& mt);

    extern std::vector < std::vector < double > > hammersleySampling(int Nd, int Ns);
    extern std::vector < std::vector < double > > haltonSampling(int Nd, int Ns);

    extern double piNumber(int Ns);
    extern std::string toLower(std::string str);
    extern std::vector<std::string> permuteCharacters(std::string topermute);
    extern std::string changeBase(std::string Base, int number);

    /******************************** End Sampling ********************************/

};

#endif /* SAMPLING_H_ */