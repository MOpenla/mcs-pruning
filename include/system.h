/*
 * system.h
 *
 *  Created on: April 27, 2017
 *      Author: MOpenlander
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <vector>
#include <string>


namespace System {
    
    /******************************** System ********************************/
    extern void loadSystemData(double& pLoad, double& qLoad, int& nb, int& nt, std::string curSystem, std::vector<Generator>& gens, std::vector<Line>& lines, std::vector<Bus>& Buses);
    /******************************** End System ******************************/

};

#endif /* SYSTEM_H_ */