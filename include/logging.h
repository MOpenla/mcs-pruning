/*
 * logging.h
 *
 *  Created on: April 27, 2017
 *      Author: MOpenlander
 */

#ifndef LOGGING_H_
#define LOGGING_H_

#include <vector>
#include <string>


namespace Logging {
    
    /******************************** Logging ********************************/
    extern void writeLineOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, char* aTime,
            bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > lineOutageCounts, std::vector<Line> lines, int numThreads);

    extern void writeGeneratorOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj, bool useLines, bool multiObj, char* aTime,
            bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > genOutageCounts, std::vector<Generator> gens, int numThreads);

    extern std::string getBaseFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            int numThreads);
                                
    extern std::string getFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness,
            double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,int numThreads);
                    
    //extern char* getTimeStamp();
    extern void getTimeStamp(char* aTime);
    /******************************** End Logging *****************************/

};

#endif /* LOGGING_H_ */