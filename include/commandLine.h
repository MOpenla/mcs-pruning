/*
 * commandLine.h
 *
 *  Created on: April 27, 2017
 *      Author: MOpenlander
 */

#ifndef COMMANDLINE_H_
#define COMMANDLINE_H_

#include <vector>
#include <string>


namespace CommandLine {
    
    /******************************** Command Line ****************************/
    extern void setUsage(AnyOption* opt);
    extern void setOptions(AnyOption* opt);
    /******************************** EndCommand Line *************************/

};

#endif /* COMMANDLINE_H_ */