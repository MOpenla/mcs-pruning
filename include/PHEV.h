/*
 * PHEV.h
 *
 *  Created on: April 27, 2017
 *      Author: MOpenlander
 */

#ifndef PHEV_H_
#define PHEV_H_

#include <vector>
#include "MTRand.h"


namespace PHEV {
    
    /******************************** PHEV ********************************/
    extern void calculatePHEVLoad(
            double penetrationLevel, double rho,
            int totalVehicles, int numBuses,
            std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT PHEV_PLACEMENT = pruning::PP_EVEN_ALL_BUSES);

    /******************************** End PHEV ********************************/

};

#endif /* PHEV_H_ */
