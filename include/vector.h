/*
 * vector.h
 *
 *  Created on: April 27, 2017
 *      Author: MOpenlander
 */

#ifndef VECTOR_H_
#define VECTOR_H_

#include <vector>
#include <string>


namespace Vector {
    
    /******************************** Vector ********************************/
    extern void printVector(std::vector < std::vector < double > >& v, std::string title, int precision = 2);
    extern void printVector(std::vector < std::vector < std::vector < double > > >& v, std::string title, int precision = 2);
    /******************************** End Vector ********************************/

};

#endif /* VECTOR_H_ */