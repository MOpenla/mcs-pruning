/*
 * string.h
 *
 *  Created on: April 27, 2017
 *      Author: MOpenlander
 */

#ifndef STRING_H_
#define STRING_H_

#include <vector>
#include <string>

namespace String {

    /******************************** String ********************************/
    extern std::string vectorToString(std::vector<double> v);
    extern std::string vectorToString(std::vector<int> v);
    extern std::string arrayToString(int* v, int size);
    extern void tokenizeString(std::string str,std::vector<std::string>& tokens,const std::string& delimiter );
    /******************************** End String ********************************/

};

#endif /* STRING_H_ */